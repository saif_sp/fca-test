import React from 'react';
import './App.css';
import RickMortySearch from './RickMortySearch';

function App() {
  return (
    <RickMortySearch></RickMortySearch>
  );
}

export default App;
