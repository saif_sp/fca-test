import axios from 'axios';

const serviceEndPoint = 'http://localhost:8080/api/character';

async function search(characterName) {
    const response = await axios.get(`${serviceEndPoint}/?name=${characterName}`)
    return response.data;
}

export default { search }