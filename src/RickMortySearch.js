import React, { Component } from 'react'
import SearchForm from './components/SearchForm'
import CharacterGrid from './components/CharacterGrid'
import RickMortyApi from './services/RickMortyApi'

import _sortBy from 'lodash/sortBy'

export default class RickMortySearch extends Component {

    constructor(props) {
        super(props)

        this.state = {
            characters: []
        }
    }

    onSearchSubmit = async (term, sortTerm = false) => {
        const response = await RickMortyApi.search(term);

        if (!!sortTerm)
            this.setState({ characters: _sortBy(response.results, [o => o[sortTerm]]) })
        else
            this.setState({ characters: response.results })
    }

    onSearchSort = term => {
        let sortedCharacters = _sortBy(this.state.characters, [o => o[term]])

        this.setState({ characters: sortedCharacters })
    }


    render() {
        return (
            <div className="ui very padded container segment">
                <h1 className="ui header">Welcome to the Rick and Morty search engine!</h1>
                <SearchForm onSubmit={this.onSearchSubmit} onSort={this.onSearchSort} />

                <CharacterGrid characters={this.state.characters}></CharacterGrid>
            </div>
        )
    }
}
