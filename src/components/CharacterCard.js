import React, { Component } from 'react'

export default class CharacterCard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            characterDetails: this.props.characterDetails
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.characterDetails !== state.characterDetails) {
            console.log(props.characterDetails)
            return { characterDetails: props.characterDetails }
        }

        // Return null to indicate no change to state.
        return null;
    }

    /* componentWillReceiveProps = newProps => {
        this.setState({ characterDetails: newProps.characterDetails })
        console.log(this.state.characterDetails)
    } */

    render() {
        return (
            <div className="column">
                <div className="ui fluid card" tabIndex="0">
                    <div className="image">
                        <img src={this.state.characterDetails.image} alt={`${this.state.characterDetails.name}`} />
                    </div>
                    <div className="content">
                        <div className="header">{this.state.characterDetails.name}</div>
                        <div className="description">
                            {this.state.characterDetails.species} / {this.state.characterDetails.gender} / {this.state.characterDetails.status}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
