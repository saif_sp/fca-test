import React, { Component } from 'react'
import CharacterCard from './CharacterCard'
import './CharacterGrid.css'

export default class CharacterGrid extends Component {

    constructor(props) {
        super(props)

        this.state = {
            characters: props.characters
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.characters !== state.characters) {
            return { characters: props.characters }
        }

        // Return null to indicate no change to state.
        return null;
    }

    /*     componentWillReceiveProps = newProps => {
            this.setState({ characters: newProps.characters })
            console.log(newProps.characters)
        } */

    render() {
        return (

            <div className="ui three column stackable grid character-grid">
                {this.state.characters.map(character => <CharacterCard characterDetails={character} key={character.id}></CharacterCard>)}
            </div>

        )
    }
}
