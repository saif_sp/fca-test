import React, { Component } from 'react'

export default class SearchForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            searchValue: '',
            searchSort: '',
        }
    }
    
    handleOnChange = (event) => {
        this.setState({ searchValue: event.target.value })
    }

    onFormSubmit = (event) => {
        event.preventDefault();

        this.props.onSubmit(this.state.searchValue, this.state.searchSort)

    }

    onFormSort = (event) => {
        this.setState({ searchSort: event.target.value })

        this.props.onSort(event.target.value)
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onFormSubmit} className="ui form">
                    <div className="fields">
                        <div className="twelve wide field">
                            <label>Enter search text:</label>
                            <input
                                type="text"
                                onChange={event => this.handleOnChange(event)}
                                value={this.state.searchValue} />
                        </div>
                        <div className="four wide field">
                            <label>Sort by:</label>
                            <select className="ui search dropdown" value={this.state.searchSort} onChange={this.onFormSort}>
                                <option value="">Select Option</option>
                                <option value="name">Name</option>
                                <option value="species">Species</option>
                                <option value="status">Status</option>
                            </select>
                        </div>
                    </div>
                    <button
                        className="ui button"
                        type="submit">Submit</button>
                </form>
            </div>
        )
    }
}
